# SSH Key Tutorial 

 ## What are SSH Keys used for: 

A Secure Socket Shell (SSH) Key is used to securely authenticate and communicate between two systems over a network. SSH Keys are a pair of cryptographic keys (Public Key and Private Key) that are used to authenticate a client to an SSH server as an alternative to password-based logins. SSH keys are used very often to login remotely from different systems because of their strong encryption.  

Why SSH Keys are important: 

In today’s era of cloud infrastructure and remote work, it is important to make sure our data is secure while it is being transferred over networks. Compared to traditional passwords, SSH keys are a more secure way of logging into a server or transferring sensitive data between computers. Other reasons to use SSH keys aside from added security include: the ability to log into servers and execute commands through automation, Single Sign-On (SSO), controlling which users are allowed to access a server, more scalable in large environments, and convenience. 

For more information on ssh keys: https://jumpcloud.com/blog/what-are-ssh-keys 

## Steps

### 1. Create the SSH key 

Open a command line and run the following command 
 
```{bash}
ssh-keygen
```

The command will output the following line: 

```{bash}
Generating public/private rsa key pair. 
Enter file in which to save the key (/home/username/.ssh/id_rsa): 
```

You will then be prompted to enter a passphrase which is recommended, but if you do not wish to set a password, you can just hit enter twice 

*Note: if you haven’t used linux before, you may notice that trying to type is a password doesn’t result in any text appearing in the terminal. This is not a glitch, but rather a feature of Linux to prevent anyone from stealing a glance at your passwords as you type them!* 

<img title="Image 1" alt="" src="./img_1.png">

To find where you ssh key is (if you went to default), just go the  C:\Users\{Your User Name} in your file explorer 

<img title="Image 2" alt="" src="./img_2.png">

<img title="Image 3" alt="" src="./img_3.png">

### 2.1 Adding SSH key to Github 

To add SSH to github, login and find the SSH menu (click your home account icon) 

<img title="Image 4" alt="" src="./img_4.png">

Once there, go to new SSH Key, and from there you copy and paste the Key from id_rsa.pub (MAKE SURE TO USE THE .pub FILE) 

Then you're all set!

For a tutorial on connecting your key to github, go to the following link: https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account 
 
### 2.2 Adding ssh key manually 

In many cases, such as setting up and ssh key on a supercomputing account (like with MSU's HPCC), you may need to set up the key manually.

1. Navigate to your ssh public key (as shown previously) and open it in any text editor 

2. Copy the contents of the file 

3. Log on to your remote environment, and make sure you’re on your home directory (you can use the ‘pwd’ command to check) 

<img title="Image 5" alt="" src="./img_5.png">

4. Navigate to your hidden ssh directory using the following command: 

```{bash}
cd .ssh 
```

<img title="Image 6" alt="" src="./img_6.png">

5. If it does not exist, run the following command first 

```{bash}
mkdir .ssh 
```

6. Once in the directory, run ls and check if the file ‘authorized_keys’ file exists. If it does not, run 

```{bash}
touch authorized_keys 
```

7. To create the file add your public key to authorized_keys. You can do this with a text editor, or with the following command 

```{bash}
echo (paste public ssh key here) >> authorized_keys 
```

With that, your ssh key should work the next time you access your device!

For a tutorial on connecting your key to the HPCC, use the following link:
https://docs.icer.msu.edu/SSH_Key-Based_Authentication/

## Debugging Notes

Make you store the password associated with the ssh key somewhere! It can’t be reset, so if you forget it then you’ll need to regenerate an entirely new key. 

A common problem is the key itself not having the requisite permissions – you can check the permissions your key has by running: 

```
ls -l ~/.ssh/id_rsa 
```